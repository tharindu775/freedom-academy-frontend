import { FooterType } from "../interfaces/Footer";

export const getFooter = async (): Promise<FooterType> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/footer`)
    return await response.json();
}