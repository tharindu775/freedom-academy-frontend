import { HomePage } from "../interfaces/HomePage";

export const getHomePage = async (): Promise<HomePage> => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_SERVER}/landing-page`, {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    return await response.json();
}