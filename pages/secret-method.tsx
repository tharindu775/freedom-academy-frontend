import Head from 'next/head';
import { Box, Text, Flex, Image, Button } from '@chakra-ui/react';
import { MethodCriteria } from '../components/secret-method/criteria/MethodCriteria';
import StudentType from '../components/secret-method/student-types/StudentType';
import { Sparrow } from '../components/secret-method/student-types/Sparrow';
import { Seagull } from '../components/secret-method/student-types/Seagull';
import { Eagle } from '../components/secret-method/student-types/Eagle';
import { motion } from 'framer-motion';

const SecretMethod = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>Il Nostro Metodo | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Text
        as="h1"
        textAlign="center"
        px={['2rem', '2rem', '2rem', '8rem', '10rem', '30rem']}
        pt={['6rem', '7rem', '8rem', '10rem', '18rem']}
        color="#001A75"
        fontWeight="600"
        fontSize={['1.14rem', '1.5rem', '2rem', '3rem']}
      >
        “La Nostra Missione E' Provvedere Una Scorciatoia Verso La Liberta'
        Economica Ad Ogni Persona Intrappolata Da Un Lavoro”
      </Text>
      <Text
        textAlign="center"
        fontWeight="600"
        mt={[5, 10]}
        as="h3"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        color="#001A75"
      >
        Ecco come lo rendiamo possibile
      </Text>
      <Text
        as="h1"
        textAlign="center"
        color="#001A75"
        fontWeight="600"
        fontSize={['1.2rem', '1.2rem', '1.75rem', '2.25rem']}
        mt={['4rem', '6rem']}
        px={[2, 4, 4, 0]}
      >
        I Nostri Metodi Di Guadagno Online Rispettano Questi Criteri...
      </Text>
      <Flex mt={['5rem', '5rem', '5rem', '8rem']} direction="column">
        <MethodCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <MethodCriteria
          line
          title="Strategia Innovativa Con Pochissima Competizione"
          description="La Nostra Strategia Di Guadagno Deve Essere Innovativa Ed Avere Poca Competizione, Questo Permette A Chi Inizia Da Zero A Guadagnare Piú Facilmente."
        />
        <MethodCriteria
          title="Minimo Sforzo, Massimo Risultato"
          description="Il Nostro Metodo Di Guadagno Online Deve Avere Un Approccio 80/20. Questo Significa Che il 20% delle azioni che compiano determinano l’80% dei risultati. Questo Ci Aiuita a Fare Meno Cose, Ma Farle Bene."
        />
      </Flex>
      <Text
        mt="8rem"
        as="h1"
        fontSize={['1.2rem', '1.5rem', '2rem', '3rem']}
        color="#001A75"
        fontWeight="600"
        textAlign="center"
        px={[3, 3, 3, 0]}
      >
        Abbiamo Tre Tipi Di Studenti
      </Text>
      <Flex
        direction="column"
        px={['2rem', '2rem', '9rem', '17rem', '24rem', '30rem']}
        mt="6rem"
      >
        <StudentType
          image={<Sparrow />}
          amount="500"
          title="Passerotto"
          description="Un Nostro Studente Diventa Ufficialmente “Passerotto” Quando Ha guadagnato I Suoi Primi 500€ Al Mese Online"
        />
        <StudentType
          image={<Seagull />}
          amount="2.000"
          title="Gabbiano"
          description="Il Nostro Studente Diventa Un Gabbiano Appena Guadagna Piú Di 2.000€  Al Mese Online!"
        />
        <StudentType
          image={<Eagle />}
          amount="5.000"
          title="Aquila"
          description="Un’Aquila É Uno Studente Che Guadagna Piú Di 5.000€  Al Mese! In Volo Verso La Libertá Economica!"
        />
      </Flex>
      <Box mt={8} position="relative">
        <Text
          as="h1"
          textAlign="center"
          color="#001A75"
          fontSize={['1.2rem', '1.5rem', '2rem', '3rem']}
          fontWeight="600"
          position="relative"
          zIndex="12"
        >
          Inizia Ora
        </Text>
        <Text
          position="relative"
          zIndex="12"
          color="#7F8E9D"
          textAlign="center"
          fontSize={['.9rem', '.9rem', '1.125rem']}
          mt={5}
          px={['2rem', '2rem', '5rem', '5rem', '5rem', '32rem']}
        >
          We built this service from scratch until now. Start working anywhere
          and anytime from now with us and get the benefits right now
        </Text>
        <Button
          position="relative"
          left="50%"
          transform="translateX(-50%)"
          marginTop="2rem"
          zIndex="15"
          textAlign="center"
          fontSize={['.55rem', '.9rem']}
          borderRadius="4px"
          background="#00F6BE"
        >
          Guarda Ora
        </Button>
        <Image
          mt={['-5rem', '-5rem', '-10rem', '-20rem']}
          w="100%"
          src="/assets/starts_now.png"
        />
      </Box>
    </motion.div>
  );
};
export default SecretMethod;
