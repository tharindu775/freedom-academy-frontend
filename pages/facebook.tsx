import Head from 'next/head';
import { Text, Button, Flex, Box, Image } from '@chakra-ui/react';
import { motion } from 'framer-motion';
import { FacebookCriteria } from '../components/facebook/FacebookCriteria';

const facebook = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>Facebook | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Text
        textAlign="center"
        fontWeight="600"
        pt="4rem"
        as="h3"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        color="#FF5B5B"
      >
        Esclusivo PDF gratuito rivela...
      </Text>
      <Text
        as="h1"
        textAlign="center"
        px={['2rem', '2rem', '2rem', '8rem', '10rem', '30rem']}
        pt="4rem"
        color="#001A75"
        fontWeight="400"
        fontSize={['1.14rem', '1.5rem', '2rem', '3rem']}
      >
        7 Cose Di Cui NON Hai Bisogno Per Avviare Un' Attività Online
      </Text>
      <Text
        textAlign="center"
        fontWeight="600"
        mt={[5, 10]}
        as="h3"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        color="#FF5B5B"
      >
        (Senza Alcuna Abilità Tecnica O Conoscenza Precedente)
      </Text>
      <Flex
        direction={['column', 'column', 'column', 'column', 'column', 'row']}
        mt={['3rem', '6rem']}
        px={['1.5rem', '1.5rem', '3rem', '15rem']}
        position="relative"
        zIndex="15"
      >
        <Box>
          <Image width="100%" src="/assets/facebook.png" />
          <Text color="#001A75" fontSize="10px" mt={4}>
            Vittorio Barbano E' Passato Da Lavorare Come Cameriere In Spagna A
            Diventare Imprenditore Online Di Successo Completamente Da Casa
            Evitando Di Fare Queste 7 Cose
          </Text>
        </Box>
        <Box
          pl={[0, 0, 0, '4rem']}
          w={['100%', '100%', '100%', '100%', '100%', '55%']}
        >
          <Text
            textAlign={['center', 'center', 'center', 'center', 'left']}
            mt={[8, 8, 8, 8, 8, -5]}
            mb={6}
            as="h1"
            fontWeight="600"
            fontSize={['1.75rem', '1.75rem', '1.75rem', '2.25rem']}
            color="#001A75"
          >
            Ecco Cosa Scoprirai:
          </Text>
          <FacebookCriteria
            boldText="Come è cambiato il business online nel 2020"
            normalText="e come tu possa sfruttare i cambiamenti recenti invece che rimanerne vittima"
          />
          <FacebookCriteria
            boldText={`3  "errori da principiante" che fanno il 95% delle persone che vogliono crearsi un'entrata online`}
            normalText="e che causano solo una perdita di tempo e denaro"
          />
          <FacebookCriteria
            boldText="Le 7 Cose Di Cui Non Hai Bisogno Per Avviare La Tua Prima Attività Online Di Successo"
            normalText="E Che Non Fanno Altro Che Rallentare Il Tuo Progresso"
          />
          <Button
            zIndex="15"
            textAlign="center"
            fontSize={['.55rem', '.9rem']}
            borderRadius="4px"
            background="#00F6BE"
            p={['1.25rem 2.75rem', '1.75rem 3.25rem']}
            marginLeft="2.5rem"
            marginTop={['-3rem', 0]}
          >
            SCARICA GUIDA
          </Button>
          <Text
            marginLeft="2.5rem"
            marginTop=".8rem"
            fontSize="14px"
            color="#001A75"
            mt={6}
          >
            100% Gratuita - Per Un Tempo Limitato
          </Text>
        </Box>
      </Flex>
      <Image
        mt={['-5rem', '-5rem', '-10rem', '-18rem']}
        w="100%"
        src="/assets/starts_now.png"
      />
      <Box p={8}>
        <Text color="#001A75" mb={6} fontSize="14px">
          Metododoppiopdf.com
        </Text>
        <Text color="#001A75" mb={6} fontSize="14px">
          Terms&conditions - Privacy
        </Text>
        <Text color="#001A75" mb={6} fontSize="14px">
          Copyright © 2020 - www.metododoppiopdf.com
        </Text>
        <Text color="#001A75" mb={6} fontSize="14px">
          Vittorio Barbano can not and does not make any guarantees about your
          ability to get results or earn any money with our ideas, information,
          tools, or strategies.{' '}
        </Text>
        <Text color="#001A75" mb={6} fontSize="14px">
          *Nothing on this page, any of our websites, or any of our content or
          curriculum is a promise or guarantee of results or future earnings,
          and we do not offer any legal, medical, tax or other professional
          advice. Any financial numbers referenced here, or on any of our sites,
          are illustrative of concepts only and should not be considered average
          earnings, exact earnings, or promises for actual or future
          performance. Use caution and always consult your accountant, lawyer or
          professional advisor before acting on this or any information related
          to a lifestyle change or your business or finances. You alone are
          responsible and accountable for your decisions, actions and results in
          life, and by your registration here you agree not to attempt to hold
          us liable for your decisions, actions or results, at any time, under
          any circumstance.
        </Text>
        <Text color="#001A75" fontSize="14px">
          This site is not a part of the Facebook website or Facebook Inc.
          Additionally, This site is NOT endorsed by Facebook in any way.
          FACEBOOK is a trademark of FACEBOOK, Inc.
        </Text>
      </Box>
    </motion.div>
  );
};

export default facebook;
