import Head from 'next/head';
import { motion } from 'framer-motion';
import { Flex, Image, Text, Box } from '@chakra-ui/react';

const history = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>La Nostra Storia | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Flex pt={['5rem', '5rem', '5rem', '15rem']} justifyContent="center">
        <Image src="/assets/history-1.png" />
      </Flex>
      <Text
        mt={8}
        as="h1"
        fontWeight="700"
        color="#001A75"
        fontSize={['1.14rem', '1.5rem', '2rem', '3rem']}
        textAlign="center"
      >
        La Storia Di Vittorio Barbano
      </Text>
      <Text
        mt={['3rem', '5rem']}
        as="h1"
        textAlign="center"
        color="#001A75"
        fontWeight="700"
        fontSize={['1rem', '1.2rem', '1.75rem', '2.25rem']}
      >
        Underpaid Waiter At € 10,000 Per Month Online.
      </Text>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          "Purtroppo non posso pagarti per la serata, <br />
          avevo calcolato male il budget per il personale, mi spiace"
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questo è stato il momento che mi ha definito come un imprenditore, ma
          torniamo indietro...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il mio viaggio imprenditoriale è iniziato ad appena 19 anni. Ero un
          ragazzo appena uscito da scuola senza una minima idea di che cosa
          avrei fatto nella vita...
        </Text>
      </Box>
      <Flex mt="3rem" justifyContent="center">
        <Image src="/assets/history-2.png" />
      </Flex>
      <Text
        as="h1"
        color="#001A75"
        fontSize={['1rem', '1.2rem', '1.75rem', '2.25rem']}
        fontWeight="600"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        mt="4rem"
        letterSpacing="2%"
      >
        Foto Scattata Subito Prima Del Mio Primissimo Colloquio Di Lavoro
      </Text>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Senza laurea, ho passato mesi cercando di trovarmi un lavoro stabile
          chemi permettesse di vivere dignitosamente.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Senza risultati...finivo sempre per fare lavoretti occasionali ma non
          riuscivo mai a trovare qualcosa di STABILE e PREVEDIBILE che mi
          pagasse tutti i mesi.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questo stile di vita non avrebbe coperto le mie spese e stavo vivendo
          continuamente con la preoccupazione di rimanere senza soldi.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Che cosa ho deciso di fare alla fine?
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ho provato a tentare la fortuna cambiando paese ed andando in Spagna a
          trovarmi un lavoro...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dopo settimane di ricerca, l'unico lavoro "stabile" che mi ero
          riuscito a trovare era come cameriere con una magra paga di 5 Euro
          l'ora.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          La parte peggiore?
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Lavoravo dalle 21 alle 6 di mattino.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ho passato mesi con lo stesso programma giornaliero.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Sveglia alle 15 (arrivavo a casa alle 7 di mattina e cercavo comunque
          di dormire 8 ore), passare le prossime 6 ore scombussolato dagli orari
          di sonno, andare al lavoro per pagare le mie spese. Arrivare a casa,
          lava via l'odore di alcohol e sigarette, andare a dormire. Ripetere.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Zero vita sociale, zero tempo per allenarsi, zero tempo per vivere la
          mia gioventú al meglio.
        </Text>
      </Box>
      <Flex mt="5rem" justifyContent="center">
        <Image src="/assets/history-3.png" />
      </Flex>
      <Text
        as="h1"
        color="#001A75"
        fontSize={['1rem', '1.2rem', '1.75rem', '2.25rem']}
        fontWeight="600"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        mt="4rem"
      >
        La Mia Magra Paga Per Giorni Di Lavoro Passati A Spaccarsi La Schiena...
      </Text>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Tutto questo ripetuto 7 giorni su sette nella magra speranza che il
          mio capo (che si chiamava Pablo) mi concedesse un giorno di riposo
          senza tagliarmi la paga.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Fino al giorno in cui finalmente ho ceduto.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Era la notte di capodanno con -5 ° (si, fa freddo anche in Spagna).
          Considerando il vento sembrava che facessero -51 °. Il mio capo mi
          aveva scritto dicendomi che aveva bisogno di aiuto nella cucina e che
          mi avrebbe pagato di piú del normale...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Nonostante tutti i miei amici mi avessero invitato ad una festa decisi
          di andare.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Perché?
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ero in ritardo con le mie bollette.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Appena arrivato nel locale il mio capo Pablo senza nemmeno augurarmi
          buon anno mi ha messo a lavare i piatti nella cucina.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ho passato 8 ore lavando i piatti. Mentre tutti erano fuori a
          divertirsi e a bere, io ero in una buia cucina a raschiare via lo
          sporco dai piatti di chi stava festeggiando.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Finalmente finisce il mio turno, sono esausto. Vedo il sole che inizia
          a sorgere.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Stanchissimo, vado da Pablo e gli chiedo di pagarmi per la notte cosí
          che possa andare a dormire.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Fu in quel momento che successe una cosa che mi fece cambiare la mia
          vita per sempre.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il mio capo Pablo mi disse una cosa che non mi dimenticheró mai.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          "Purtroppo non posso pagarti per la serata,
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          avevo calcolato male il budget per il personale, mi spiace"
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          ...Non riuscivo a credere alle sue parole, e iniziai a spiegargli che
          avevo bisogno di quei soldi e che non avrei potuto pagare l'affitto se
          non fossi stato pagato.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          "Se non ti va bene puoi anche licenziarti. Ci vediamo."
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Mi ricordo bene quella notte.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Mi ricordo come uscii correndo dal locale con le lacrime agli occhi (e
          non é facile per me ammetterlo...).
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Com'era possibile? Avevo lavorato TUTTO capodanno e non avevo NIENTE
          tra le mani.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Quello fu il momento in cui decisi che non avrei MAI PIÚ lavorato per
          un altro uomo.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Decisi che, COSTI QUEL CHE COSTI, avrei trovato un modo per guadagnare
          online e non dover dipendere mai piú da nessun altro uomo.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Qualcosa dentro di me era cambiato.
        </Text>
      </Box>
      <Flex mt="5rem" justifyContent="center">
        <Image src="/assets/history-4.png" />
      </Flex>
      <Text
        as="h1"
        color="#001A75"
        fontSize={['1rem', '1.2rem', '1.75rem', '2.25rem']}
        fontWeight="600"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        mt="4rem"
      >
        La Mia Piccola Stanza In Affitto In Spagna Dove Vivevo
      </Text>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il giorno dopo appena sveglio mi fiondo davanti al computer e vado
          subito su Amazon a cercare libri che spiegassero come guadagnare
          online...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Tra la miriade di libri che trovo uno in particolare attirava la mia
          attenzione...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questo libro parlava del MIGLIOR metodo di guadagno online per un
          principiante...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ed era descritto come «La migliore strategia che un completo
          principiante può usare per crearsi un’entrata mensile online
          velocemente e raggiungere la libertà economica»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Che dire? Ero entusiasta!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Quindi...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Compro subito il libro!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il libro mi arriva 2 giorni dopo e lo divoro tutto in 24h...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Che dire…Ero ELETTRIZZATO!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avevo capito bene come funzionava questa strategia e sentivo che mi
          avrebbe cambiato la vita!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questa strategia di guadagno consisteva nel caricare semplici
          documenti PDF su Amazon che venivano poi venduti come libri!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          A quanto pare questa era una vera e propria miniera d’oro del guadagno
          online e pubblicare libri su Amazon stava cambiando la vita a migliaia
          di persone in America...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Strano vero? Di tutti i modi che esistevano per guadagnare online a
          quanto pare pubblicare libri su Amazon era il modo migliore per un
          completo principiante
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questa era la volta buona…lo sentivo!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Mi ci sono buttato subito.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Non volevo spendere altri soldi e tempo per imparare il business così
          decido
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          di provarci da solo
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Spesi il 90% di tutti i miei risparmi per commissionare il mio primo
          libro…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E dopo due settimane lo pubblicai!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Subito dopo avrei iniziato a vedere i primi guadagni giusto?
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Beh…qualche settimana dopo feci un'amara realizzazione
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          A quanto pare avevo sbagliato tutto…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il mio libro NON AVEVA VENDUTO NEMMENO UNA COPIA in 2 settimane!!!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ed avevo investito il 90% dei miei risparmi!!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dire che ero disperato era poco…avevo perso il lavoro e pure il 90%
          dei miei risparmi cercando di trovare un modo per guadagnare online da
          solo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il mio primo libro era stato UNA TOTALE PERDITA…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ero DEVASTATO
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avevo toccato il punto più basso della mia vita…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avevo perso tutti i risparmi nel processo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Non avevo guadagnato un centesimo
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E’ come se avessi sbattuto la mia testa contro un muro…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ora mi trovavo senza lavoro e senza soldi da parte…
        </Text>
      </Box>
      <Flex mt="3rem" justifyContent="center">
        <Image src="/assets/history-5.png" />
      </Flex>
      <Text
        as="h1"
        color="#001A75"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        fontWeight="600"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        mt="4rem"
      >
        In Quel Periodo Particolarmente Difficile Della Mia Vita Uscivo Da Solo
        A Camminare Pensando A Come Avrei Potuto Ribaltare La Mia Situazione...
      </Text>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avevo due opzioni…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Arrendermi e passare i prossimi 50 anni in agonia
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Cercare di capire CHE COSA AVESSI SBAGLIATO…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dopo essermi fatto forza di nuovo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Decisi di prendere in mano lo stesso libro che avevo comprato giusto
          qualche settimana prima per rileggerlo e cercare di capire dove
          diavolo avessi sbagliato…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Solo che questa volta notai una cosa molto particolare…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Lo stavo leggendo con così tanta attenzione che notai che nell’ultima
          pagina in basso a destra c’era una piccola nota lasciata dall’autore…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «Se avete qualsiasi domanda scrivetemi pure a questa mail!»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Non so perché…ho sentito l’impulso di scrivergli una mail…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          In fondo cosa ci potevo perdere????
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Era qualcosa di molto disperato del tipo «Ti prego insegnami a
          guadagnare online, sono disperato, ho perso il lavoro…non so cosa
          fare…nessuno mi assume…se mi insegni ti pagherò quello che vuoi!!!!!»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          La disperazione fa fare cose interessanti alle persone
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Se non avessi trovato un modo di guadagnare entro 30 giorni non avrei
          potuto pagare l’affitto…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Qualche giorno dopo mentre stavo controllando le mie mail per vedere
          se qualcuno avesse risposto all’invio del mio curriculum ho visto che
          l’autore del libro mi aveva risposto…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il messaggio che mi aveva scritto mi aveva alquanto stupito…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «Ciao Vittorio…ho venduto più di 20.000 copie del mio libro e NESSUNO
          ha mai avuto l’audacia di scrivermi chiedendo aiuto e offrendo
          volontariamente di pagarmi. Dammi il tuo numero whatsapp e ti chiamerò
          domani.»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Chi avrebbe mai detto che questa sarebbe stata l’email che mi avrebbe
          cambiato la vita per sempre???
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dopo aver parlato per 1h con l’autore del libro ho scoperto una nuova
          opportunità…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Se produci il tuo libro in una certa maniera, NON PUOI FARE CHE
          vendere centinaia di copie ogni singolo mese…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          L’autore del libro stava guadagnando
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          30.000$ al mese pubblicando libri fatti in una maniera molto
          specifica…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          OGNI SINGOLO LIBRO che questa persona pubblicava gli faceva un minimo
          di 500$ al mese…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E veniva comprato da migliaia di persone…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il modo in cui questa persona pubblicava i libri era COMPLETAMENTE
          diversa da come avevo imparato a pubblicare io…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Questa persona inoltre produceva libri a costo molto, molto basso!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          L’autore mi disse…So che sembra strano, ma c’è un errore che tutte le
          persone stanno facendo e fa fallire tutti MISERAMENTE…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Tutti credono che pubblicare un libro su Amazon sia difficile, in
          realtà sarà una delle cose più semplici che farai, SE SAI COSA FARE
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «NON PROVARCI DA SOLO. PERDERAI MIGLIAIA DI EURO ED IL TUO PREZIOSO
          TEMPO»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «Voglio imparare!!! Ti prego insegnami a fare quello che stai facendo
          tu…», risposi.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «Va bene, sono 3000€...», disse a sua volta l'autore....
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Appena mi disse quella cifra iniziai a sentire le farfalle nel mio
          stomaco…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Non starò a mentirti…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          ero molto spaventato…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Però sentivo che dovevo farlo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avevo letto da qualche parte la frase…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          «Se vuoi ottenere le cose che non hai mai ottenuto, devi fare le cose
          che non hai mai fatto…non avere mai paura di investire in te stesso»
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          D’altronde quella persona stava facendo quello che volevo fare io con
          successo…e mi avrebbe insegnato a fare lo stesso…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Così ho fatto una delle cose più pazze che abbia mai fatto nella mia
          vita…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ho chiamato amici…familiari…chiunque potessi…e mi sono fatto fare un
          prestito…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ero spaventatissimo… Ma qualcosa dentro di me mi diceva di farlo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Così ho pagato a quella persona l’ intera cifra tutta in una volta!
        </Text>
      </Box>
      <Flex px={[4, 4, 0]} justifyContent="center" mt={['2rem', '3rem']}>
        <Image src="/assets/history-6.png" />
      </Flex>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E ho fatto questo screenshot per riguardarlo in futuro e ricordarmi di
          una cosa…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Non importava come sarebbe andata...almeno ci avevo provato.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Mi sarei potuto guardare allo specchio con orgoglio.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Avrei sempre saputo che sono andato la fuori e ho investito più di
          quello che potevo per cambiare la mia vita
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E almeno non avrei avuto ALCUN RIMORSO andando avanti. ALMENO AVEVO
          AGITO.
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dopo aver pagato l’autore mi svelò subito la sua strategia e mi disse
          di prendere appunti ed applicarla subito…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Così decisi subito di mettermi al lavoro…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Dopo 3 giorni il mio primo libro era pronto…decido di pubblicare il
          libro… mi era costato in totale 70$
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Carico i 2 PDF su Amazon e dopo 10 minuti il libro era LIVE in vendita
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Poi è successo qualcosa di incredibile…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il giorno dopo mi sveglio e controllo il mio computer…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Il mio libro aveva venduto 2 copie!!!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ed il giorno dopo 4…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Poi 6
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Poi 100…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E Poi…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ecco cosa è successo…
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          IL MIO PRIMO MESE AVEVO FATTO 1800$ DI PROFITTO…
        </Text>
      </Box>
      <Flex px={[4, 4, 0]} justifyContent="center" mt="3rem">
        <Image src="/assets/history-chart-1.png" />
      </Flex>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ed il mio libro continuava a vendere!!! Quindi ho pubblicato altri 2
          PDF IL MESE DOPO AVEVO FATTO PIU’ DI 2400$...
        </Text>
      </Box>
      <Flex px={[4, 4, 0]} justifyContent="center" mt="3rem">
        <Image src="/assets/history-chart-2.png" />
      </Flex>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          E poi sono arrivato addirittura a guadagnare 11.000$ al mese!!
        </Text>
      </Box>
      <Flex px={[4, 4, 0]} justifyContent="center" mt="3rem">
        <Image src="/assets/history-7.png" />
      </Flex>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ho subito lasciato la Spagna ed ho fatto il giro del mondo…
          Continuando a guadagnare dal mio computer…
        </Text>
      </Box>
      <Flex
        direction={['column', 'column', 'column', 'row']}
        justifyContent="center"
        mt="3rem"
      >
        <Image src="/assets/history-8.png" />
        <Image src="/assets/history-9.png" />
      </Flex>
      <Box
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ora mi dedico ad aiutare altre persone a raggiungere la stessa libertá
          economica che sono riuscito ad ottenere io.
        </Text>
      </Box>
      <Flex justifyContent="center" mt="3rem">
        <Image src="/assets/history-10.png" />
      </Flex>
      <Box
        position="relative"
        zIndex="15"
        mt="2rem"
        px={['2rem', '2rem', '4rem', '8rem', '10rem', '29rem']}
        color="#001A75"
      >
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ti racconto la mia storia per farti capire che...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Se c'e l'ha fatta un cameriere sottopagato di 19 anni senza laurea...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          Ce la puoi benissimo fare anche tu!
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          P.S: Se hai vuoi altro materiale gratuito e risorse varie...
        </Text>
        <Text mt={6} fontWeight="300" fontSize={['.9rem', '.9rem', '1.125rem']}>
          clicca QUI
        </Text>
      </Box>
      <Image
        mt={['-3rem', '-3rem', '-6rem', '-14rem', '-18rem']}
        w="100%"
        src="/assets/starts_now.png"
      />
    </motion.div>
  );
};

export default history;
