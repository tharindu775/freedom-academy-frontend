import Head from 'next/head'
import { GetStaticProps } from 'next'
import { Landing } from '../components/home/landing'
import { About } from '../components/home/about'
import { Content, Explore } from '../components/home'
import { motion } from 'framer-motion'
import { HomePage } from '../interfaces/HomePage'
import { getHomePage } from '../services/home'

const Home = (props: HomePage) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>Home | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Landing
        first_header={props.first_header}
        second_header={props.second_header}
        button={props.button.text}
        sub_header={props.sub_header}
      />
      <About testimonial={props.testimonial} />
      {props.image_content.map((x) => (
        <Content
          key={x.id}
          imgPath={`${process.env.NEXT_PUBLIC_SERVER}${x.image.url}`}
          imgOrder={x.order}
          title={x.title}
          firstPara={x.first_paragraph}
          secondPara={x.second_paragraph}
        />
      ))}
      <Explore
        explore_image={props.explore_image.url}
        explore={props.explore}
      />
    </motion.div>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const data = await getHomePage()
  return {
    props: {
      testimonial: data.testimonial,
      first_header: data.first_header,
      second_header: data.second_header,
      sub_header: data.sub_header,
      button: data.button,
      image_content: data.image_content,
      explore: data.explore,
      explore_image: data.explore_image,
    },
  }
}

export default Home
