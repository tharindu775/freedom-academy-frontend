import Head from 'next/head';
import { Text, Flex, Image } from '@chakra-ui/react';
import { motion } from 'framer-motion';
import { ResourceCriteria } from '../components/resource/ResourceCriteria';

const resource = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>Risorse | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Text
        as="h1"
        textAlign="center"
        px={['2rem', '2rem', '2rem', '8rem', '10rem', '30rem']}
        pt={['6rem', '7rem', '8rem', '10rem', '18rem']}
        color="#001A75"
        fontWeight="600"
        fontSize={['1.14rem', '1.5rem', '2rem', '3rem']}
      >
        “La Nostra Missione E' Provvedere Una Scorciatoia Verso La Liberta'
        Economica Ad Ogni Persona Intrappolata Da Un Lavoro”
      </Text>
      <Text
        textAlign="center"
        fontWeight="600"
        mt={[5, 10]}
        as="h3"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        color="#001A75"
      >
        Ecco come lo rendiamo possibile
      </Text>
      <Text
        as="h1"
        textAlign="center"
        color="#001A75"
        fontWeight="600"
        fontSize={['1.2rem', '1.2rem', '1.75rem', '2.25rem']}
        mt={['4rem', '6rem']}
        px={[2, 4, 4, 0]}
      >
        I Nostri Metodi Di Guadagno Online Rispettano Questi Criteri...
      </Text>
      <Flex
        position="relative"
        zIndex="15"
        mt={['5rem', '5rem', '5rem', '8rem']}
        direction="column"
      >
        <ResourceCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <ResourceCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <ResourceCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <ResourceCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <ResourceCriteria
          line
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
        <ResourceCriteria
          title="Strategia Di Guadagno Adatta A Chi Parte Da Zero"
          description="La Nostra Strategia Di Guadagno Deve Essere Adatta A Chi Parte Da Zero Senza Alcuna Esperienza Pregressa Né Un Grande Capitale"
        />
      </Flex>
      <Image
        mt={['-5rem', '-5rem', '-10rem', '-20rem']}
        w="100%"
        src="/assets/starts_now.png"
      />
    </motion.div>
  );
};

export default resource;
