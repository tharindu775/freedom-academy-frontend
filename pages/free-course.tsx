import Head from 'next/head';
import { Text, Button, Flex, Box, Image } from '@chakra-ui/react';
import { motion } from 'framer-motion';
import { CourseCriteria } from '../components/free-course/CourseCriteria';

const FreeCourse = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.7 }}
      exit={{ opacity: 0 }}
    >
      <Head>
        <title>Corso Gratuito | Freedom Academy</title>
        <meta name="description" content="Freedom Academy" />
      </Head>
      <Text
        as="h1"
        textAlign="center"
        px={['2rem', '2rem', '2rem', '8rem', '10rem', '30rem']}
        pt={['6rem', '7rem', '8rem', '10rem', '18rem']}
        color="#001A75"
        fontWeight="600"
        fontSize={['1.14rem', '1.5rem', '2rem', '3rem']}
      >
        <Text as="span" fontWeight="300">
          Ecco Come Ho Usato L'Ignoto Sistema "Doppio PDF" Per Creare
        </Text>{' '}
        Entrate Mensili Dal Mio Computer In Maniera Costante E Senza Doverci
        Mettere La Faccia
      </Text>
      <Text
        textAlign="center"
        fontWeight="600"
        mt={[5, 10]}
        as="h3"
        fontSize={['.8rem', '1rem', '1rem', '1.5rem']}
        color="#FF5B5B"
      >
        (Senza Alcuna Abilità Tecnica O Conoscenza Precedente)
      </Text>
      <Button
        position="relative"
        left="50%"
        transform="translateX(-50%)"
        marginTop="2rem"
        zIndex="15"
        textAlign="center"
        fontSize={['.55rem', '.9rem']}
        borderRadius="4px"
        background="#00F6BE"
        p={['1.75rem 3.25rem']}
      >
        ACCEDI ORA
      </Button>
      <Flex mt="8rem" px={['12rem']}>
        <Image width="100%" src="/assets/free_course.png" />
        <Box px={['4rem']}>
          <Text
            mt={-5}
            mb={6}
            as="h1"
            fontWeight="600"
            fontSize={['2.25rem']}
            color="#001A75"
          >
            Ecco Cosa Scoprirai:
          </Text>
          <CourseCriteria />
          <CourseCriteria />
        </Box>
      </Flex>
      {/* <Image
        mt={['-5rem', '-5rem', '-10rem', '-20rem']}
        w="100%"
        src="/assets/starts_now.png"
      /> */}
    </motion.div>
  );
};

export default FreeCourse;
