import { useState } from 'react'
import { AppProps } from 'next/app'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import '../style.scss'
import { AppContext } from '../AppContext'
import { Layout } from '../components/Layout'
import { AnimatePresence } from 'framer-motion'

const theme = extendTheme({
  fonts: {
    body: 'Poppins',
  },
})

const App: NextPage<AppProps> = ({ Component, pageProps }) => {
  const router = useRouter()
  const [showMobNav, setShowMobNav] = useState(false)

  return (
    <ChakraProvider theme={theme}>
      <AppContext.Provider value={{ showMobNav, setShowMobNav }}>
        <AnimatePresence>
          {router.pathname === '/facebook' ? (
            <Component {...pageProps} />
          ) : (
            <Layout>
              <Component {...pageProps} />
            </Layout>
          )}
        </AnimatePresence>
      </AppContext.Provider>
    </ChakraProvider>
  )
}

export default App
