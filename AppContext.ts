import { createContext, Dispatch, SetStateAction } from 'react';

interface Context {
    showMobNav: boolean;
    setShowMobNav: Dispatch<SetStateAction<boolean>>;
}

const AppContext = createContext<Context>({} as Context);

export { AppContext };