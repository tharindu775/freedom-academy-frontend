export interface HomePage {
    first_header: string;
    second_header: string;
    sub_header: string;
    button: {
        text: string;
    }
    testimonial: Testimonial;
    image_content: Array<ImageContent>;
    explore: Explore;
    explore_image: {
        url: string;
    }
}

export interface Explore {
    id: number;
    title: string;
    description: string;
    text: {
        text: string;
    }
}

export interface ImageContent {
    id: number;
    title: string;
    first_paragraph: string;
    second_paragraph: string;
    order?: number;
    image: {
        url: string;
    }
}

export interface Testimonial {
    testimonial_header: string;
    testimonial_text: string;
    testimonial_card: Array<TestimonialCard>;
}

export interface TestimonialCard {
    id: number;
    client: string,
    status: string,
    testimonial_content: string,
    image: {
        url: string;
    }
}