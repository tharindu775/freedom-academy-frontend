export interface FooterType {
    logo: {
        url: string;
    }
    copyright: string;
    footer_list: Array<{
        id: number;
        title: string;
        link: Array<{
            id: number,
            name: string,
            url: string
        }>
    }>
    footer_social: {
        header_text: string;
        social_images: Array<{
            id: number;
            url: string;
        }>
    }
}