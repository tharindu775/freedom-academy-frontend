import { FC } from 'react'
import { Box } from '@chakra-ui/react'
import { Navigation } from './navigation/Navigation'
import { Footer } from './Footer'

const Layout: FC = ({ children }) => {
  return (
    <Box position="relative">
      <Navigation />
      {children}
      <Footer />
    </Box>
  )
}

export { Layout }
