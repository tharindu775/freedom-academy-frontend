import { FC, useMemo, useState } from 'react'
import { Box, Flex, Text, Image } from '@chakra-ui/react'
import Link from 'next/link'
import { FooterType } from '../interfaces/Footer'
import { getFooter } from '../services/footer'

const Footer: FC = () => {
  const [loading, setLoading] = useState(true)
  const [footer, setFooter] = useState<FooterType>()
  useMemo(() => {
    getFooter().then((res) => {
      setFooter(res)
      setLoading(false)
    })
  }, [])
  return (
    <Flex
      px={[4, 4, 4, 5, 7]}
      py={20}
      flexDirection={['column', 'column', 'column', 'row']}
      justifyContent="space-around"
      alignItems={['center', 'center', 'center', 'start']}
    >
      {loading ? (
        'Loading'
      ) : (
        <>
          <Box>
            <Flex justifyContent="center">
              <Image
                src={`${process.env.NEXT_PUBLIC_SERVER}${footer.logo.url}`}
                width="60%"
              />
            </Flex>
            <Text
              textAlign={['center', 'center', 'left']}
              color="#7F8E9D"
              fontSize={['.8rem', '.8rem', '.9rem', '.9rem', '1.1rem']}
              mt={[4, 4, 4, 10]}
              px={[0, 0, 0, 0, '1.2rem']}
            >
              {footer.copyright}
            </Text>
          </Box>
          {footer.footer_list.map((l) => (
            <Box key={l.id} textAlign={['center', 'center', 'center', 'start']}>
              <Text
                fontSize={['1rem', '1.1rem', '1.3rem']}
                mt={[4, 4, 4, 0]}
                color="#0B0052"
              >
                {l.title}
              </Text>
              <Flex
                fontSize={['.8rem', '.8rem', '.9rem', '.9rem', '1.1rem']}
                mt={3}
                flexDirection="column"
                color="#7F8E9D"
              >
                {l.link.map((link) => (
                  <Link key={link.id} href={link.url}>
                    <Text py={2}>{link.name}</Text>
                  </Link>
                ))}
              </Flex>
            </Box>
          ))}
          <Flex flexDirection={['column']} alignItems="center">
            <Box>
              <Text
                mt={[3, 3, 3, 0]}
                textAlign={['center', 'center', 'center', 'left']}
                color="#0B0052"
                fontSize={['1rem', '1.1rem', '1.3rem']}
              >
                TORNA IN CIMA
              </Text>
              <Text
                mt={[3, 3, 3, 8]}
                fontSize={['.8rem', '.8rem', '.9rem', '.9rem', '1.1rem']}
                textAlign={['center', 'center', 'center', 'left']}
                color="#7F8E9D"
              >
                {footer.footer_social.header_text}
              </Text>
            </Box>
            <Flex
              mt={[6, 6, 8]}
              w="50%"
              justifyContent="space-around"
              marginLeft={[0, 0, 0, -4]}
              marginRight={[0, 0, 0, 'auto']}
            >
              {footer.footer_social.social_images.map((i) => (
                <img
                  key={i.id}
                  src={`${process.env.NEXT_PUBLIC_SERVER}${i.url}`}
                />
              ))}
            </Flex>
          </Flex>
        </>
      )}
    </Flex>
  )
}
export { Footer }
