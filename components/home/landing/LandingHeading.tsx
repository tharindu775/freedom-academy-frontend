import styled from '@emotion/styled';

const LandingHeading = styled.h1`
  position: absolute;
  top: 22%;
  left: 17.5%;
  letter-spacing: 2px;
  font-size: 2.5rem;
  color: #001a75;
  font-weight: 600;
  @media screen and (max-width: 75em) {
    top: 24%;
    font-size: 1.9rem;
  }
  @media screen and (max-width: 62em) {
    top: 24%;
    font-size: 1.5rem;
  }
  @media screen and (max-width: 52em) {
    top: 26%;
    font-size: 1.2rem;
  }
  @media screen and (min-width: 30em) and (max-width: 52em) {
    font-size: 1rem;
    letter-spacing: 0;
  }
  @media screen and (max-width: 30em) {
    font-size: 0.8rem;
    letter-spacing: 0;
  }
`;

export { LandingHeading };
