import { FC } from 'react'
import { Box, Image } from '@chakra-ui/react'
import { LandingHeading, LandingTag, LandingBtn } from '.'

interface LandingProps {
  first_header: string
  second_header: string
  sub_header: string
  button: string
}

const Landing: FC<LandingProps> = (props) => {
  return (
    <Box position="relative">
      <Image src="/assets/background.png" width="100vw" />
      <LandingHeading>
        {props.first_header}
        <br />
        {props.second_header}
      </LandingHeading>
      <LandingTag>{props.sub_header}</LandingTag>
      <LandingBtn>{props.button}</LandingBtn>
    </Box>
  )
}

export { Landing }
