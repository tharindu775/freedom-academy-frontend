import styled from '@emotion/styled';

const LandingBtn = styled.button`
  position: absolute;
  top: 40.5%;
  left: 17.5%;
  background: #9680ff;
  border-radius: 0.3125rem;
  color: #fff;
  font-weight: 500;
  font-size: 0.9375rem;
  padding: 0.75rem 3.625rem;
  border: none;
  @media screen and (max-width: 78em) {
    padding: 0.5rem 3rem;
  }
  @media screen and (max-width: 62em) {
    font-size: 0.8rem;
    padding: 0.4rem 2.8rem;
  }
  @media screen and (max-width: 52em) {
    top: 42%;
    font-size: 0.8rem;
    padding: 0.4rem 2.6rem;
  }
  @media screen and (min-width: 75em) {
    top: 37%;
  }
  @media screen and (min-width: 30em) and (max-width: 52em) {
    /* top: 46%; */
    font-size: 0.6rem;
    padding: 0.4rem 2.6rem;
  }
  @media screen and (max-width: 30em) {
    top: 55%;
    font-size: 0.5rem;
    padding: 0.2rem 1.6rem;
  }
`;

export { LandingBtn };
