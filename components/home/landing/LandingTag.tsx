import styled from '@emotion/styled';

const LandingTag = styled.p`
  position: absolute;
  top: 35%;
  left: 17.5%;
  font-size: 1.1875rem;
  color: #3c638a;
  font-weight: 500;
  @media screen and (min-width: 75em) {
    top: 32%;
  }
  @media screen and (max-width: 75em) {
    font-size: 1rem;
  }
  @media screen and (max-width: 62em) {
    font-size: 0.9rem;
  }
  @media screen and (max-width: 52em) {
    top: 36%;
    font-size: 0.9rem;
  }
  @media screen and (min-width: 30em) and (max-width: 52em) {
    /* top: 38%; */
    font-size: 0.84rem;
  }
  @media screen and (max-width: 30em) {
    font-size: 0.7rem;
    top: 44%;
    letter-spacing: 0;
  }
`;

export { LandingTag };
