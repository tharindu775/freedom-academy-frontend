import { Button } from '@chakra-ui/button'
import { Image } from '@chakra-ui/image'
import { Box, Flex, Text } from '@chakra-ui/layout'
import { FC } from 'react'
import { Explore as ExploreType } from '../../interfaces/HomePage'

interface ExploreProps {
  explore: ExploreType
  explore_image: string
}

const Explore: FC<ExploreProps> = (props) => {
  return (
    <Box position="relative">
      <Flex
        position="relative"
        zIndex="15"
        marginTop={['7%', '7%', '6%', '10%']}
        marginBottom={['2%', '2%', '-3%', '-10%']}
        marginLeft={['0%', '8%']}
        flexDirection="column"
        w={['100%', '70%', '70%', '50%', '40%']}
        alignItems="center"
      >
        <Text
          fontSize={['1rem', '2rem', '2.4rem', '3rem']}
          color="#001A75"
          fontWeight="600"
        >
          {props.explore.title}
        </Text>
        <Text
          fontSize={['.8rem', '1.2rem', '1.4rem', '1.6rem']}
          px={['2rem']}
          mt={4}
          textAlign="center"
          color="#7F8E9D"
        >
          {props.explore.description}
        </Text>
        <Button
          fontSize={['.55rem', '.9rem']}
          marginTop={4}
          px={['.5rem']}
          borderRadius="4px"
          background="#00F6BE"
        >
          {props.explore.text.text}
        </Button>
      </Flex>
      <Image
        w="100%"
        src={`${process.env.NEXT_PUBLIC_SERVER}${props.explore_image}`}
      />
    </Box>
  )
}

export { Explore }
