import { FC } from 'react';
import { Flex, Image, Text } from '@chakra-ui/react';

interface ContentProps {
  imgPath: string;
  title: string;
  firstPara: string;
  secondPara: string;
  imgOrder?: number;
  ml?: string;
}

const Content: FC<ContentProps> = (props) => {
  return (
    <Flex
      flexDirection={['column', 'column', 'column', 'row']}
      justifyContent="center"
    >
      <Image
        order={[0, 0, 0, props.imgOrder]}
        src={props.imgPath}
        marginLeft={props.ml}
        width={['50%', '50%', '50%', '40%']}
      />
      <Flex
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
        padding={['1rem', '2rem']}
      >
        <Text
          color="#001A75"
          fontWeight="600"
          fontSize={['1rem', '1.5rem', '2.2rem']}
          letterSpacing=".02rem"
          width={['80%']}
        >
          {props.title}
        </Text>
        <Text
          width="80%"
          fontSize={['.7rem', '1rem', '1.2rem']}
          mt={8}
          color="#7F8E9D"
        >
          {props.firstPara}
        </Text>
        <Text
          width="80%"
          fontSize={['.7rem', '1rem', '1.2rem']}
          mt={5}
          color="#7F8E9D"
        >
          {props.secondPara}
        </Text>
      </Flex>
    </Flex>
  );
};

export { Content };
