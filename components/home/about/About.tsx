import { FC } from 'react'
import { Box } from '@chakra-ui/layout'
import styled from '@emotion/styled'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Navigation } from 'swiper/core'
import 'swiper/swiper.min.css'
import 'swiper/components/navigation/navigation.min.css'
import { Card } from './'
import { Testimonial } from '../../../interfaces/HomePage'

SwiperCore.use([Navigation])

const AboutHeading = styled.h1`
  text-align: center;
  color: #001a75;
  font-weight: 600;
  font-size: 3rem;
  letter-spacing: 0.02rem;
  line-height: 72px;
  @media screen and (max-width: 62em) {
    font-size: 2.3rem;
    margin-bottom: 2rem;
    line-height: 0;
  }
  @media screen and (max-width: 48em) {
    font-size: 1.6rem;
    margin-bottom: 2rem;
    line-height: 0;
  }
  @media screen and (max-width: 30em) {
    font-size: 1.1rem;
    margin-bottom: 1rem;
    line-height: 0;
  }
`

const AboutDescription = styled.p`
  color: #7f8e9d;
  letter-spacing: 0.02rem;
  line-height: 28px;
  text-align: center;
  padding: 0 10rem;
  @media screen and (min-width: 75em) {
    padding: 0 20rem;
  }
  @media screen and (max-width: 62em) {
    padding: 0 3rem;
  }
  @media screen and (max-width: 30em) {
    padding: 0 1rem;
    font-size: 0.8rem;
  }
`

interface AboutProps {
  testimonial: Testimonial
}

const About: FC<AboutProps> = (props) => (
  <Box
    position="relative"
    zIndex="10"
    mt={['-1rem', '-3rem', '-9rem', '-12rem']}
    p={['0', '1rem', '5rem']}
  >
    <AboutHeading>{props.testimonial.testimonial_header}</AboutHeading>
    <AboutDescription>{props.testimonial.testimonial_text}</AboutDescription>
    <Swiper
      navigation={true}
      slidesPerView={1}
      spaceBetween={10}
      breakpoints={{
        '480': {
          slidesPerView: 1,
          spaceBetween: 30,
        },
        '678': {
          slidesPerView: 2,
          spaceBetween: 15,
        },
        '1200': {
          slidesPerView: 3,
          spaceBetween: 30,
        },
      }}
      style={{ padding: '2rem' }}
    >
      {props.testimonial.testimonial_card.map((t, i) => (
        <SwiperSlide key={i}>
          <Card
            url={`${process.env.NEXT_PUBLIC_SERVER}${t.image.url}`}
            name={t.client}
            status={t.status}
            description={t.testimonial_content}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  </Box>
)

export { About }
