import { FC } from 'react'
import styled from '@emotion/styled'

const Container = styled.div`
  padding: 1.5rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  box-shadow: 7px 21px 26px 0px #101d4a1a;
  border-radius: 10px;
  min-height: 16rem;
  max-height: 18rem;
  @media screen and (max-width: 75em) {
    min-height: 18rem;
  }
`

const Description = styled.p`
  font-size: 1rem;
  color: #7f8e9d;
  padding-bottom: 1.5rem;
  border-bottom: 1px solid #e7e7e7;
  flex: 1;
  overflow-y: auto;
  @media screen and (max-width: 30em) {
    font-size: 0.8rem;
  }
`

const Author = styled.div`
  display: flex;
  margin-top: 0.7rem;
  margin-bottom: -0.4rem;
  height: 3rem;
`

const Image = styled.div`
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
`

const Info = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 1.6rem;
  h4 {
    color: #0b0052;
    font-weight: 600;
    letter-spacing: 0.02rem;
    line-height: 27px;
    @media screen and (max-width: 30em) {
      font-size: 0.8rem;
    }
  }
  p {
    color: #7f8e9d;
    letter-spacing: 0.02rem;
    line-height: 28px;
    font-size: 1rem;
    @media screen and (max-width: 30em) {
      font-size: 0.8rem;
    }
  }
`

interface CardProps {
  description: string
  name: string
  status: string
  url: string
}

const Card: FC<CardProps> = ({ description, name, status, url }) => {
  return (
    <Container>
      <Description>{description}</Description>
      <Author>
        <Image
          style={{
            background: `url(${url})`,
            backgroundPosition: 'center center',
          }}
        />
        <Info>
          <h4>{name}</h4>
          <p>{status}</p>
        </Info>
      </Author>
    </Container>
  )
}

export { Card }
