import { FC } from 'react';

const VectorLine: FC = () => {
  return (
    <svg
      style={{ margin: '2rem 0' }}
      width="2"
      height="178"
      viewBox="0 0 2 178"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect width="2" height="178" fill="#14E0B2" />
      <rect width="2" height="178" stroke="#00BEF8" />
    </svg>
  );
};

export { VectorLine };
