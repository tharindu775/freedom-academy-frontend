import { FC } from 'react';
import { Image, Flex } from '@chakra-ui/react';

const Seagull: FC = () => (
  <Flex direction="column">
    <Image src="/assets/seagull.png" mb="1.2rem" />
    <svg
      width="143"
      height="7"
      viewBox="0 0 143 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 3.5C1 1.84315 2.34315 0.5 4 0.5H142V6.5H4C2.34315 6.5 1 5.15685 1 3.5Z"
        fill="#00F6BE"
        stroke="#00BEF8"
      />
    </svg>
  </Flex>
);

export { Seagull };
