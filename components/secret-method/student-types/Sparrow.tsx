import { FC } from 'react';
import { Image, Flex } from '@chakra-ui/react';

const Sparrow: FC = () => (
  <Flex direction="column">
    <Image src="/assets/sparrow.png" mb="1.2rem" />
    <svg
      width="77"
      height="7"
      viewBox="0 0 77 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M1 3.5C1 1.84315 2.34315 0.5 4 0.5H76V6.5H4C2.34315 6.5 1 5.15685 1 3.5Z"
        fill="#00F6BE"
        stroke="#00BEF8"
      />
    </svg>
  </Flex>
);

export { Sparrow };
