import { FC } from 'react';
import { Flex, Text } from '@chakra-ui/react';

interface StudentTypeProps {
  image: JSX.Element;
  amount: string;
  title: string;
  description: string;
}

const StudentType: FC<StudentTypeProps> = ({
  image,
  amount,
  title,
  description,
}) => {
  return (
    <Flex direction={['column', 'column', 'row']} mb="3rem">
      <Flex
        w={['100%', '100%"', 0]}
        direction="column"
        alignItems={['center', 'center', 'flex-start']}
      >
        <Flex
          ml={[0, 0, 10]}
          w={['flex', 'flex', '10rem']}
          direction="column"
          alignItems="flex-end"
        >
          {image}
          <Text
            position="relative"
            zIndex="15"
            mt=".8rem"
            color="#001A75"
            fontSize={['.9rem', '1.125rem']}
            fontWeight="600"
          >
            {amount}€ Al Mese
          </Text>
        </Flex>
      </Flex>
      <Flex
        px={[4, 4, 4, 0]}
        pl={[0, 0, '4rem', '5rem']}
        mt={[4, 4, 0, 0]}
        direction="column"
        alignItems="flex-start"
      >
        <Text
          as="h3"
          color="#001A75"
          fontSize={['1.2rem', '1.5rem']}
          fontWeight="600"
        >
          {title}
        </Text>
        <Text color="#7F8E9D" fontSize={['.9rem', '1.125rem']}>
          {description}
        </Text>
      </Flex>
    </Flex>
  );
};

export default StudentType;
