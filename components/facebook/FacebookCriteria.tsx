import { FC } from 'react';
import { Flex, Text } from '@chakra-ui/react';
import { VectorCircle } from './VectorCircle';

interface FacebookCriteriaProps {
  boldText: string;
  normalText: string;
}

const FacebookCriteria: FC<FacebookCriteriaProps> = ({
  boldText,
  normalText,
}) => {
  return (
    <Flex mb="4rem">
      <VectorCircle />
      <Text
        mt=".45rem"
        ml={3}
        as="div"
        color="#001A75"
        fontWeight="300"
        fontSize={['14px', '14px', '14px', '16px']}
      >
        <Text as="span" fontWeight="500">
          {boldText}
        </Text>{' '}
        {normalText}
      </Text>
    </Flex>
  );
};

export { FacebookCriteria };
