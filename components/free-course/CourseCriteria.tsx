import { FC } from 'react';
import { Flex, Text } from '@chakra-ui/react';
import { VectorCircle } from './VectorCircle';

const CourseCriteria: FC = () => {
  return (
    <Flex mb={4}>
      <VectorCircle />
      <Text
        mt=".45rem"
        ml={3}
        as="div"
        color="#001A75"
        fontWeight="500"
        fontSize={['14px']}
      >
        <Text as="span" fontWeight="300">
          Come è cambiato il business online nel 2020
        </Text>{' '}
        e come tu possa sfruttare i cambiamenti recenti invece che rimanerne
        vittima
      </Text>
    </Flex>
  );
};

export { CourseCriteria };
