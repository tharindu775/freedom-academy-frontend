import styled from '@emotion/styled';

const NavList = styled.ul`
  list-style: none;
  display: flex;
  justify-content: space-evenly;
  @media screen and (max-width: 62em) {
    display: none;
  }
`;

export { NavList };
