import styled from '@emotion/styled';

const ListItem = styled.li`
  color: #0f006a;
  font-size: 0.9rem;
  padding: 0 0.55rem;
  &:not(:last-child) {
    border-right: 1px solid #0f006a;
  }
  @media screen and (min-width: 75em) {
    font-size: 1rem;
  }
  @media screen and (max-width: 75em) {
    font-size: 0.75rem;
  }
`;

export { ListItem };
