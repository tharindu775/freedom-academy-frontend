import { useContext } from 'react';
import Link from 'next/link';
import { Box, Image } from '@chakra-ui/react';
import { BiMenuAltRight } from 'react-icons/bi';
import { NavigationWrapper } from './NavigationWrapper';
import { NavList } from './NavList';
import { ListItem } from './ListItem';
import { ItemLink } from './ItemLink';
import { NavBtn } from './NavBtn';
import { MobileNav } from './MobileNav';
import { AppContext } from '../../AppContext';

const Navigation = () => {
  const { showMobNav, setShowMobNav } = useContext(AppContext);

  return (
    <>
      {showMobNav && <MobileNav />}
      <NavigationWrapper>
        <Link href="/">
          <a>
            <div className="brand">
              <Image
                src="/assets/logo.png"
                width={['30%', '50%', '50%', '55%', '75%']}
              />
            </div>
          </a>
        </Link>
        <NavList>
          <ListItem>
            <ItemLink href="/history">La Nostra Storia</ItemLink>
          </ListItem>
          <ListItem>
            <ItemLink href="/secret-method">Il Nostro Metodo</ItemLink>
          </ListItem>
          <ListItem>
            <ItemLink href="/resource">Risorse</ItemLink>
          </ListItem>
          <ListItem>
            <ItemLink href="/">Corso Gratuito</ItemLink>
          </ListItem>
          <ListItem>
            <ItemLink href="/">Recensioni</ItemLink>
          </ListItem>
        </NavList>
        <NavBtn>Diventa Cliente</NavBtn>
        <Box
          display={['block', 'block', 'block', 'none']}
          onClick={() => setShowMobNav((s) => !s)}
        >
          <BiMenuAltRight cursor="pointer" />
        </Box>
      </NavigationWrapper>
    </>
  );
};
export { Navigation };
