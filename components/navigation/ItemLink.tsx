import Link from 'next/link';
import styled from '@emotion/styled';

const ItemLink = styled(Link)`
  text-decoration: none;
  font-weight: 400;
`;

export { ItemLink };
