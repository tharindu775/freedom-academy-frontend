import { FC, useContext } from 'react';
import Link from 'next/link';
import { Box, Flex } from '@chakra-ui/react';
import { motion, AnimatePresence } from 'framer-motion';
import { AiOutlineClose } from 'react-icons/ai';
import { AppContext } from '../../AppContext';

const MobileNav: FC = () => {
  const { setShowMobNav } = useContext(AppContext);

  return (
    <motion.div
      initial={{
        position: 'fixed',
        height: '100vh',
        width: '0',
        background: '#fff',
        zIndex: 99,
        opacity: 0,
      }}
      animate={{ opacity: 1, width: '100vw' }}
      transition={{ duration: '.5' }}
      exit={{ opacity: 0, width: '0' }}
    >
      <Flex
        justifyContent="flex-end"
        p={5}
        onClick={() => setShowMobNav((s) => !s)}
      >
        <AiOutlineClose color="#000" fontSize="1.4rem" />
      </Flex>
      <Box
        position="absolute"
        top="40%"
        left="50%"
        transform="translate(-50%, -50%)"
        alignItems="center"
        color="#0F006A"
      >
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: '.7', delay: 0.7 }}
        >
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/">Home</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/history">La Nostra Storia</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/secret-method">Il Nostro Metodo</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/resource">Risorse</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/">Corso Gratuito</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/">Recensioni</Link>
          </Box>
          <Box py={4} onClick={() => setShowMobNav((s) => !s)}>
            <Link href="/">Diventa Cliente</Link>
          </Box>
        </motion.div>
      </Box>
    </motion.div>
  );
};

export { MobileNav };
