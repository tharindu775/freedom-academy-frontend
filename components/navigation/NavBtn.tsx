import styled from '@emotion/styled';

const NavBtn = styled.button`
  padding: 0.2rem 0.4rem;
  background: transparent;
  border: 3px solid #9680ff;
  border-radius: 2px;
  color: #0f006a;
  outline: none;
  box-shadow: none;
  font-weight: 400;
  @media screen and (max-width: 75em) {
    font-size: 0.75rem;
  }
  @media screen and (max-width: 62em) {
    display: none;
  }
`;

export { NavBtn };
