import styled from '@emotion/styled';

const NavigationWrapper = styled.nav`
  position: absolute;
  z-index: 10;
  width: 85vw;
  top: 6rem;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
  justify-content: space-around;
  @media screen and (max-width: 62em) {
    justify-content: space-between;
    align-items: center;
    top: 3.5rem;
  }
  @media screen and (max-width: 48em) {
    justify-content: space-between;
    align-items: center;
    top: 3rem;
  }
  @media screen and (max-width: 30em) {
    justify-content: space-between;
    align-items: center;
    top: 2rem;
  }
`;

export { NavigationWrapper };
