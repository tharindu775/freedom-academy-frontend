import { FC } from 'react';
import { Flex, Text, Image } from '@chakra-ui/react';
import { VectorLine } from './VectorLine';

interface CriteriaProps {
  line?: boolean;
  title: string;
  description: string;
}

const ResourceCriteria: FC<CriteriaProps> = ({ line, title, description }) => {
  return (
    <Flex justify="center" align="start" px={['1.7rem', '1.7rem', '1.7rem', 0]}>
      <Flex
        mt={3}
        width="9rem"
        direction="column"
        align="center"
        mr={[4, 4, 4, 6]}
      >
        <Image src="/assets/resource.png" />
        {line && <VectorLine />}
      </Flex>
      <Flex direction="column" w={['flex', '70%', '50%', '36%']}>
        <Text
          as="h3"
          fontSize={['1rem', '1.2rem', '1.2rem', '1.5rem']}
          color="#001A75"
          fontWeight="600"
        >
          {title}
        </Text>
        <Text color="#7F8E9D" fontSize={['.9rem', '1rem', '1rem', '1.125rem']}>
          {description}
        </Text>
      </Flex>
    </Flex>
  );
};

export { ResourceCriteria };
